#include "Bubble.h"

Bubble::Bubble()
{
	radius = 60;
	color = sf::Color(100, 250, 50);
	centerX = 0;
	centerY = 0;
	drawToggle = true;
	render = sf::CircleShape (radius, 30);
	render.setFillColor(color);
	render.setOrigin(radius, radius);
}

Bubble::~Bubble()
{

}

void Bubble::draw(sf::RenderWindow& destination)
{
	if(drawToggle = true)	{
		destination.draw(render);
	}
	
}

void Bubble::setRadius(int r)
{
	radius = r;
}


void Bubble::setColor(sf::Color c)
{
	color = c;
}

int Bubble::getRadius()
{
	return radius;
}


sf::Color Bubble::getColor()
{
	return color;
}

//void setPosition(int x, int y)
//{
//	centerX = x;
//	centerY = y;
//	render.setPosition(centerX,centerY)
//}

//sf::CircleShape Bubble::getRender()
//{
//	return render;
//}