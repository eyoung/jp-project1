#ifndef JP_BUBBLE_H
#define JP_BUBBLE_H

#include <SFML/Graphics.hpp>

class Bubble
{
public:
	Bubble();
	~Bubble();

	void setRadius(int r);
	void setColor(sf::Color c);
	void setPosition(int x, int y);
	void draw(sf::RenderWindow& destination);
	int getRadius();
	int getRenderID();
	sf::Color getColor();
	sf::CircleShape render;
	

private:
	int radius;
	int centerX;
	int centerY;
	sf::Color color;
	bool drawToggle;
};

#endif