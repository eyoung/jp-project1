#ifndef JP_SQUARE_H
#define JP_SQUARE_H

#include <SFML/Graphics.hpp>

class Square
{
public:
    Square();
    ~Square();

    void setWidth(int w);
    void setHeight(int h);
    void setColor(sf::Color c);
    int getWidth();
    int getHeight();
    sf::Color getColor();
 
private:
    int width;
    int height;
    sf::Color color;
};

#endif