
#include <SFML/Window.hpp>
#include "Square.h"
#include "Bubble.h"
#include <iostream>
#include <vector>

void clickHandle(sf::CircleShape& circle, sf::Event& clickevent);

int main() 
{
	auto testBubble = Bubble();

	sf::RenderWindow window(sf::VideoMode(800, 600), "jp-project1");
	

    while (window.isOpen())
    {
        sf::Event event;

        while (window.pollEvent(event))
        {
            if(event.type == sf::Event::Closed) 
			{
                window.close();
                std::cout << "Closing window" << std::endl;
            }
			else if(event.type == sf::Event::MouseButtonPressed)
			{
				clickHandle(testBubble.render, event);
			}
        }
		window.clear(sf::Color ::White);
		testBubble.draw(window);
        window.display();

    }
    return 0;
}

void clickHandle(sf::CircleShape& circle, sf::Event& clickevent)
{
	circle.setPosition(clickevent.mouseButton.x, clickevent.mouseButton.y);
}