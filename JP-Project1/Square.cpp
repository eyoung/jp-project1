#include "Square.h"

Square::Square()
{
    width = 120;
    height = 120;
    color = sf::Color(100,250,50);
}

Square::~Square()
{

}

void Square::setWidth(int w)
{
    width = w;
}

void Square::setHeight(int h)
{
    height = h;
}

void Square::setColor(sf::Color c)
{
    color = c;
}

int Square::getWidth()
{
    return width;
}

int Square::getHeight()
{
    return height;
}

sf::Color Square::getColor()
{
    return color;
}